#!/usr/bin/env filebot -script

// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
// Script Name: AMC-Plus
// Version: 	1.0.13
// Created By: 	Ctark
// Tested By:	Clamps
// Credits: 	RedNoah - orig script
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
// Script Usage: (Modify to suit your needs and add to your uTorrent settings)
//
// "C:/Program Files/FileBot/filebot.launcher.exe" -script "G:/Downloads/Links/amc.groovy" --output "G:/Downloads/Links" --log-file "G:/Downloads/Links/amc.log" --action hardlink --conflict auto -non-strict --def fromUT=true --def excludeList="G:/Downloads/Links/amc-input.txt" "ut_label=%L" "ut_state=%S" "ut_tracker=%T" "ut_title=%N" "ut_kind=%K" "ut_file=%F" "ut_dir=%D"
//
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//     Set Custom Stuff Here!
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
UsePlex		= false						// Set this to true if you use Plex
PlexHost	= '127.0.0.1'				// Change this only if you don't have Plex on this computer
PlexPort	= '32400'					// Change this only if Plex isn't on the default port
PlexToken	= 'CHANGE_ME_TO_PLEX_TOKEN'	// Add your Plex token here
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//      End of Custom Stuff!
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=



// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
// Boring Stuff Below...
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

// Silly imports here
import static net.filebot.media.XattrMetaInfo.*;
import net.filebot.MetaAttributeView;
import net.filebot.Resource;
// log input parameters
log.fine("Run script [$_args.script] at [$now]")
_def.each{ n, v -> log.finest('Parameter: ' + [n, n =~ /plex|kodi|pushover|pushbullet|mail|myepisodes/ ? '*****' : v].join(' = ')) }
args.withIndex().each{ f, i -> if (f.exists()) { log.finest "Argument[$i]: $f" } else { log.warning "Argument[$i]: File does not exist: $f" } }

// initialize variables
failOnError = _args.conflict.equalsIgnoreCase('fail')
testRun = _args.action.equalsIgnoreCase('test')

// --output folder must be a valid folder
outputFolder = tryLogCatch{ any{ _args.output }{ '.' }.toFile().getCanonicalFile() }

// enable/disable features as specified via --def parameters
//unsorted  = tryQuietly{ unsorted.toBoolean() }
unsorted	= true
//music		= tryQuietly{ music.toBoolean() }
music 		= true
subtitles	= tryQuietly{ subtitles.split(/\W+/) as List }
artwork		= tryQuietly{ artwork.toBoolean() && !testRun }
extras		= tryQuietly{ extras.toBoolean() }
clean		= tryQuietly{ clean.toBoolean() }
exec		= tryQuietly{ exec.toString() }
fromUT		= tryQuietly{ fromUT.toBoolean() }
showskipped 	= tryQuietly{ showskipped.toBoolean() }

// array of kodi/plex/emby hosts
kodi = tryQuietly{ any{kodi}{xbmc}.split(/[ ,;|]+/)*.split(/:(?=\d+$)/).collect{ it.length >= 2 ? [host: it[0], port: it[1] as int] : [host: it[0]] } }
plex = tryQuietly{ plex.split(/[ ,;|]+/)*.split(/:/).collect{ it.length >= 2 ? [host: it[0], token: it[1]] : [host: it[0]] } }
emby = tryQuietly{ emby.split(/[ ,;|]+/)*.split(/:/).collect{ it.length >= 2 ? [host: it[0], token: it[1]] : [host: it[0]] } }

// extra options, myepisodes updates and email notifications
extractFolder		= tryQuietly{ extractFolder as File }
skipExtract			= tryQuietly{ skipExtract.toBoolean() }
deleteAfterExtract	= tryQuietly{ deleteAfterExtract.toBoolean() }
excludeList			= tryQuietly{ def f = excludeList as File; f.isAbsolute() ? f : outputFolder.resolve(f.path) }
myepisodes			= tryQuietly{ myepisodes.split(':', 2) as List }
gmail				= tryQuietly{ gmail.split(':', 2) as List }
mail				= tryQuietly{ mail.split(':', 5) as List }
pushover			= tryQuietly{ pushover.split(':', 2) as List }
pushbullet			= tryQuietly{ pushbullet.toString() }
storeReport			= tryQuietly{ storeReport.toBoolean() }
reportError			= tryQuietly{ reportError.toBoolean() }

// user-defined filters
label		= any{ _args.mode }{ ut_label }{ null }
tracker		= any{ ut_tracker }{ null }
ignore		= any{ ignore }{ null }
minFileSize	= any{ minFileSize.toLong() }{ 50 * 1000L * 1000L }
minLengthMS	= any{ minLengthMS.toLong() }{ 10 * 60 * 1000L }

// include artwork/nfo, pushover/pushbullet and ant utilities as required
if (artwork || kodi || plex || emby) { include('fn:lib/htpc') }
if (pushover || pushbullet ) { include('fn:lib/web') }
if (gmail || mail) { include('fn:lib/ant') }

// series/anime/movie format expressions
seriesFormat	= any{ seriesFormat		}{ '''TV Shows/{n}/{episode.special ? 'Special' : 'Season '+s.pad(2)}/{n} - {episode.special ? 'S00E'+special.pad(2) : s00e00} - {vf} - {t.replaceAll(/[`´‘’ʻ]/, /'/).replaceAll(/[!?.]+$/).replacePart(', Part $1')}{'.'+lang}''' }
animeFormat		= any{ animeFormat		}{ '''TV Shows/{primaryTitle}/{primaryTitle} - {sxe} - {vf} - {t.replaceAll(/[!?.]+$/).replaceAll(/[`´‘’ʻ]/, /'/).replacePart(', Part $1')}''' }
movieFormat		= any{ movieFormat		}{ '''Movies/{n} ({y})/{n} ({y}) - {vf}{' CD'+pi}{'.'+lang}''' }
musicFormat		= any{ musicFormat		}{ '''Music/{n}/{album+'/'}{pi.pad(2)+'. '}{artist} - {t}''' }
unsortedFormat	= any{ unsortedFormat	}{ 'Unsorted/{file.structurePathTail}' }
rioFormat		= any{ rioFormat		}{ '''Rio Olympics/{file.structurePathTail}''' }
ebookFormat		= any{ ebookFormat		}{ '''eBooks/{file.structurePathTail}''' }
gamesFormat		= any{ gamesFormat		}{ '''Games/{file.structurePathTail}''' }
comicsFormat	= any{ comicsFormat		}{ '''Comics/{file.structurePathTail}''' }

// Force Formats
force_cli_rio	= false
forceEbooks		= tryQuietly{ febook.toBoolean() 	}
forceGames		= tryQuietly{ fgames.toBoolean() 	}
forceMusic		= tryQuietly{ fmusic.toBoolean() 	}
forceTV			= tryQuietly{ ftv.toBoolean() 		}
forceMovies		= tryQuietly{ fmovies.toBoolean() 	}
forceComics 	= tryQuietly{ fcomic.toBoolean() 	}

// Stupid Var init's:
force_games = false
force_movies = false
force_ebook = false
force_music = false
force_tv = false

if (forceEbooks) {
	force_cli_ebook = true
	log.fine("Forcing eBooks due to CLI Flag")
} else {
	force_cli_ebook = false
}
if (forceGames) {
	force_cli_games = true
	log.fine("Forcing Games due to CLI Flag")
} else {
	force_cli_games = false
}
if (forceMusic) {
	force_cli_music = true
	log.fine("Forcing Music due to CLI Flag")
} else {
	force_cli_music = false
}
if (forceTV) {
	force_cli_tv = true
	log.fine("Forcing TV due to CLI Flag")
} else {
	force_cli_tv = false
}
if (forceMovies) {
	force_cli_movies = true
	log.fine("Forcing Movies due to CLI Flag")
} else {
	force_cli_movies = false
}
if (forceComics) {
	force_cli_comics = true
	log.fine("Forcing Comics due to CLI Flag")
} else {
	force_cli_comics = false
}

def trackerHost = null
def trackercheck(f) {
	if (fromUT) {
		def trackerURL = new URL(tracker)
		trackerHost = trackerURL.host
		log.fine("Hostname for Tracker: $trackerHost")	
		
		f.xattr['net.filebot.tracker'] = trackerHost
		if (label) {
			f.xattr['net.filebot.label'] = label
		}
	} else {
		def label = f.xattr['net.filebot.label']
		trackerHost = f.xattr['net.filebot.tracker']
		
		if (label) {
			log.finest "xattr_extra for $f.name: [Label] => [$label]"
		}
		if (trackerHost) {
			log.finest "xattr_extra for $f.name: [Tracker] => [$trackerHost]"
		}
	}

	if (trackerHost) {
		if (trackerHost == "landof.tv") { 
			force_tv = true 
			log.fine("Forcing TV due to Tracker: $trackerHost")
		}
		if (trackerHost == "please.passtheheadphones.me" || trackerHost == "flacsfor.me" || trackerHost == "redacted.me") { 
			force_music = true 
			log.fine("Forcing Music due to Tracker: $trackerHost")
		}
		if (trackerHost == "www.thegeeks.bz") { 
			force_ebook = true 
			log.fine("Forcing eBooks for $f.name due to Tracker: $trackerHost")
		}
		if (trackerHost == "please.passthepopcorn.me") { 
			force_movies = true 
			log.fine("Forcing Movies due to Tracker: $trackerHost")
		}
		if (trackerHost == "gazelgamers.net") { 
			force_games = true 
			log.fine("Forcing Games due to Tracker: $trackerHost")
		}
	} else {
		force_games = false
		force_movies = false
		force_ebook = false
		force_music = false
		force_tv = false
	}
}

	// force Movie / TV Series / Anime behaviour

	def forceRio(f) { 
		label =~ /^(?i:RIO)/ || force_cli_rio
	}
	def forceGames(f) { 
		label =~ /^(?i:games|game)/ || force_games || force_cli_games
	}
	def forceComics(f) { 
		label =~ /^(?i:comic|comics)/ || force_cli_comics
	}
	def forceMovie(f) {
		label =~ /^(?i:Movie|Film|Concert|UFC|PTP)/ || f.dir.listPath().any{ it.name ==~ /(?i:Movies)/ } || f.isMovie() || force_movies || force_cli_movies
	}
	def forceSeries(f) {
		label =~ /^(?i:TV|Show|Series|Documentary)/ || f.dir.listPath().any{ it.name ==~ /(?i:TV.Shows)/ } || f.path =~ /(?<=\b|_)(?i:tv[sp]-|Season\D?\d{1,2}|\d{4}.S\d{2})(?=\b|_)/ || parseEpisodeNumber(f.path, true) || parseDate(f.path) || force_cli_tv || force_tv
	}
	def forceAnime(f) {
		label =~ /^(?i:Anime)/ || f.dir.listPath().any{ it.name ==~ /(?i:Anime)/ } || (f.isVideo() && (f.name =~ /(?i:HorribleSubs)/ || f.name =~ /[\(\[]\p{XDigit}{8}[\]\)]/ || any{ getMediaInfo(f, '{media.AudioLanguageList} {media.TextCodecList}').tokenize().containsAll(['Japanese', 'ASS']) && (parseEpisodeNumber(f.name, false) != null || getMediaInfo(f, '{minutes}').toInteger() < 60) }{ false }))
	}
	def forceAudio(f) {
		label =~ /^(?i:audio|music|music.video|PTH)/ || (f.isAudio() && !f.isVideo()) || force_cli_music || force_music
	}
	def forceEbook(f) { 
		label =~ /^(?i:eBook)/ || force_cli_ebook || force_ebook
	}
	def forceIgnore(f) {
		label =~ /^(?i:other|ignore)/
	}


// error reporting functions
def sendEmailReport(title, message, messagetype) {
	if (gmail) {
		sendGmail(
			subject: title, message: message, messagemimetype: messagetype,
			to: any{ mailto } { gmail[0].contains('@') ? gmail[0] : gmail[0] + '@gmail.com' },		// mail to self by default
			user: gmail[0].contains('@') ? gmail[0] : gmail[0] + '@gmail.com', password: gmail[1]
		)
	}
	if (mail) {
		sendmail(
			subject: title, message: message, messagemimetype: messagetype,
			mailhost: mail[0], mailport: mail[1], from: mail[2], to: mailto,
			user: mail[3], password: mail[4]
		)
	}
}

def fail(message) {
	if (reportError) {
		sendEmailReport('[FileBot] Failure', message as String, 'text/plain')
	}
	die(message)
}



// check input parameters
def ut = _def.findAll{ k, v -> k.startsWith('ut_') }.collectEntries{ k, v ->
	if (v ==~ /[%$]\p{Alnum}|\p{Punct}+/) {
		log.warning "Bad $k value: $v"
		v = null
	}
	return [k.substring(3), v ? v : null]
}



// sanity checks
if (outputFolder == null || !outputFolder.isDirectory()) {
	fail "Illegal usage: output folder must exist and must be a directory: $outputFolder"
}

if (ut.dir) {
	if (ut.state_allow && !(ut.state ==~ ut.state_allow)) {
		fail "Illegal state: $ut.state != $ut.state_allow"
	}
	if (args.size() > 0) {
		fail "Illegal usage: use either script parameters $ut or file arguments $args but not both"
	}
	if (ut.dir == '/') {
		fail "Illegal usage: No! Are you insane? You can't just pass in the entire filesystem. Think long and hard about what you just tried to do."
	}
	if (outputFolder.path.startsWith(ut.dir)) {
		fail "Illegal usage: output folder [$outputFolder] must be separate from input folder $ut"
	}
} else if (args.size() == 0) {
	fail "Illegal usage: no input"
} else if (args.any{ f -> outputFolder.path.startsWith(f.path) }) {
	fail "Illegal usage: output folder [$outputFolder] must be separate from input arguments $args"
} else if (args.any{ f -> f in File.listRoots() }) {
	fail "Illegal usage: input $args must not include a filesystem root"
}



// collect input fileset as specified by the given --def parameters
roots = args

if (args.size() == 0) {
	// assume we're called with utorrent parameters (account for older and newer versions of uTorrents)
	if (ut.kind == 'single' || (ut.kind != 'multi' && ut.dir && ut.file)) {
		roots = [new File(ut.dir, ut.file).getCanonicalFile()] // single-file torrent
	} else {
		roots = [new File(ut.dir).getCanonicalFile()] // multi-file torrent
	}
}

// helper function to work with the structure relative path rather than the whole absolute path
def relativeInputPath(f) {
	def r = roots.find{ r -> f.path.startsWith(r.path) && r.isDirectory() && f.isFile() }
	if (r != null) {
		return f.path.substring(r.path.length() + 1)
	}
	return f.name
}



// define and load exclude list (e.g. to make sure files are only processed once)
excludePathSet = new FileSet()

if (excludeList) {
	if (excludeList.exists()) {
		try {
			excludePathSet.load(excludeList)
		} catch(Exception e) {
			fail "Failed to load excludeList: $e"
		}
		log.fine "Use excludes: $excludeList (${excludePathSet.size()})"
	} else {
		log.fine "Use excludes: $excludeList"
		if ((!excludeList.parentFile.isDirectory() && !excludeList.parentFile.mkdirs()) || (!excludeList.isFile() && !excludeList.createNewFile())) {
			fail "Failed to create excludeList: $excludeList"
		}
	}
}


extractedArchives = []
temporaryFiles = []

def extract(f) {
	def folder = new File(extractFolder ?: f.dir, f.nameWithoutExtension)
	def files = extract(file: f, output: folder.resolve(f.dir.name), conflict: 'auto', filter: { it.isArchive() || it.isVideo() || it.isSubtitle() || (music && it.isAudio()) }, forceExtractAll: true) ?: []

	extractedArchives += f
	temporaryFiles += folder
	temporaryFiles += files

	return files
}


def acceptFile(f) {
	if (f.isHidden()) {
		if (showskipped) {
			log.finest "Ignore hidden: $f"
		}
		return false
	}

	if (f.isDirectory() && f.name ==~ /[.@].+|bin|initrd|opt|sbin|var|dev|lib|proc|sys|var.defaults|etc|lost.found|root|tmp|etc.defaults|mnt|run|usr|System.Volume.Information/) {
		if (showskipped) {
			log.finest "Ignore system path: $f"
		}
		return false
	}

	if (f.name =~ /(?<=\b|_)(?i:sample|trailer|extras|music.video|scrapbook|behind.the.scenes|extended.scenes|deleted.scenes|mini.series|s\d{2}c\d{2}|S\d+EXTRA|\d+xEXTRA|NCED|NCOP|(OP|ED)\d+|Formula.1.\d{4})(?=\b|_)/) {
		if (!forceEbook(f)) {
			if (showskipped) {
				log.finest "Ignore extra: $f"
			}
			return false
		}
	}

	// ignore if the user-defined ignore pattern matches
	if (f.path.findMatch(ignore)) {
		if (showskipped) {
			log.finest "Ignore pattern: $f"
		}
		return false
	}

	// ignore archives that are on the exclude path list
	if (excludePathSet.contains(f)) {
		return false
	}

	// accept archives if the extract feature is enabled
	if (f.isArchive() || f.hasExtension('001')) {
		return !skipExtract
	}

	// ignore iso images that do not contain a video disk structure
	if (f.hasExtension('iso') && !f.isDisk()) {
		if (showskipped) {
			log.fine "Ignore disk image: $f"
		}
		return false
	}

	// ignore small video files
//	if (minFileSize > 0 && f.isVideo() && f.length() < minFileSize) {
//		log.fine "Skip small video file: $f"
//		return false
//	}

	// ignore short videos
//	if (minLengthMS > 0 && f.isVideo() && any{ getMediaInfo(f, '{duration}').toLong() < minLengthMS }{ false /* default if MediaInfo fails */ }) {
//		log.fine "Skip short video: $f"
//		return false
//	}

	// ignore subtitle files without matching video file in the same or parent folder
	if (f.isSubtitle() && ![f, f.dir].findResults{ it.dir }.any{ it.listFiles{ it.isVideo() && f.isDerived(it) }}) {
		if (showskipped) {
			log.fine "Ignore orphaned subtitles: $f"
		}
		return false	
	}

	// process only media files (accept audio files only if music mode is enabled)
//	return f.isDirectory() || f.isVideo() || f.isSubtitle() || (music && f.isAudio())
	return true
}


// specify how to resolve input folders, e.g. grab files from all folders except disk folders and already processed folders (i.e. folders with movie/tvshow nfo files)
def resolveInput(f) {
	// resolve folder recursively, except disk folders
	if (f.isDirectory()) {
		if (f.isDisk()) {
			return f
		}
		return f.listFiles{ acceptFile(it) }.collect{ resolveInput(it) }
	}

	if (f.isArchive() || f.hasExtension('001')) {
		return extract(f).findAll{ acceptFile(it) }.collect{ resolveInput(it) }
	}

	return f
}



// flatten nested file structure
def input = roots.findAll{ acceptFile(it) }.flatten{ resolveInput(it) }.toSorted()

// update exclude list with all input that will be processed during this run
if (excludeList && !testRun) {
	excludePathSet.append(excludeList, extractedArchives, input)
}

// print exclude and input sets for logging
input.each{ log.fine "Input: $it" }

// early abort if there is nothing to do
if (input.size() == 0) {
	log.warning "No files selected for processing"
	return
}

// group episodes/movies and rename according to Plex standards
def groups = input.groupBy{ f ->
	// print xattr metadata
	if (f.metadata) {
		log.finest "xattr: [$f.name] => [$f.metadata]"
	} 

	
	// Check if we know what type of file it is and force it
	trackercheck(f)
	
	
	// skip auto-detection if possible
	if (forceIgnore(f))
		return []
	if (forceRio(f)) 
		return [rio: f.dir.name]		
	if (forceGames(f)) 
		return [games: f.dir.name]	
	if (forceComics(f)) 
		return [comics: f.dir.name]	
	if (music && forceAudio(f)) // process audio only if music mode is enabled
		return [music: f.dir.name]
	if (forceMovie(f))		
		return [mov:   detectMovie(f, false)]
	if (forceEbook(f)) 
		return [ebook: f.dir.name]	
	if (forceSeries(f))
		return [tvs:   detectSeriesName(f) ?: detectSeriesName(input.findAll{ s -> f.dir == s.dir && s.isVideo() })]
	if (forceAnime(f))
		return [anime: detectAnimeName(f) ?: detectAnimeName(input.findAll{ s -> f.dir == s.dir && s.isVideo() })]


	def tvs = detectSeriesName(f)
	def mov = detectMovie(f, false)
	log.fine "$f.name [series: $tvs, movie: $mov]"

	// DECIDE EPISODE VS MOVIE (IF NOT CLEAR)
	if (tvs && mov) {
		def norm = { s -> s.ascii().normalizePunctuation().lower().space(' ') }
		def dn = norm(guessMovieFolder(f)?.name ?: '')
		def fn = norm(f.nameWithoutExtension)
		def sn = norm(tvs)
		def mn = norm(mov.name)
		def my = mov.year as String

		// S00E00 | 2012.07.21 | One Piece 217 | Firefly - Serenity | [Taken 1, Taken 2, Taken 3, Taken 4, ..., Taken 10]
		def metrics = [
			[tvs: -1, mov:  0, fun: { mn == fn } ],
			[tvs: -1, mov:  0, fun: { mov.year >= 1950 && f.listPath().reverse().take(3).find{ it.name.contains(my) && parseEpisodeNumber(it.name.after(my), false) == null } } ],
			[tvs: -1, mov:  0, fun: { mn =~ sn && [dn, fn].find{ it =~ /\b(19|20)\d{2}\b/ && parseEpisodeNumber(it.after(/\b(19|20)\d{2}\b/), false) == null } } ],
			[tvs:  5, mov: -1, fun: { parseEpisodeNumber(fn, true) || parseDate(fn) } ],
			[tvs:  5, mov: -1, fun: { f.dir.listFiles{ it.isVideo() && (dn =~ sn || norm(it.name) =~ sn) && it.name =~ /\d{1,3}/}.findResults{ it.name.matchAll(/\d{1,3}/) as Set }.unique().size() >= 10 } ],
			[tvs:  1, mov: -1, fun: { fn.after(sn) ==~ /.{0,3}\s-\s.+/ && matchMovie(fn) == null } ],
			[tvs:  1, mov: -1, fun: { [dn, fn].find{ it =~ sn && matchMovie(it) == null } && (parseEpisodeNumber(stripReleaseInfo(fn.after(sn), false), false) || stripReleaseInfo(fn.after(sn), false) =~ /\D\d{1,2}\D{1,3}\d{1,2}\D/) && matchMovie(fn) == null } ],
			[tvs: -1, mov:  1, fun: { fn =~ /tt\d{7}/ } ],
			[tvs: -1, mov:  1, fun: { f.nameWithoutExtension ==~ /[\D\s_.]+/ } ],
			[tvs: -1, mov:  5, fun: { detectMovie(f, true) && [dn, fn].find{ it =~ /(19|20)\d{2}/ } != null } ],
			[tvs: -1, mov:  1, fun: { fn.contains(mn) && parseEpisodeNumber(fn.after(mn), false) == null } ],
			[tvs: -1, mov:  1, fun: { mn.getSimilarity(fn) >= 0.8 || [dn, fn].find{ it.findAll( ~/\d{4}/ ).findAll{ y -> [mov.year-1, mov.year, mov.year+1].contains(y.toInteger()) }.size() > 0 } != null } ],
			[tvs: -1, mov:  1, fun: { [dn, fn].find{ it =~ mn && !(it.after(mn) =~ /\b\d{1,3}\b/) && (it.getSimilarity(mn) > 0.2 + it.getSimilarity(sn)) } != null } ],
			[tvs: -1, mov:  1, fun: { detectMovie(f, false).aliasNames.find{ fn.contains(norm(it)) } } ]
		]

		def score = [tvs: 0, mov: 0]		
		metrics.each{
			if (tvs && mov && it.fun()) {
				score.tvs += it.tvs
				score.mov += it.mov

				if (score.tvs >= 1 && score.mov <= -1) {
					log.fine "Exclude Movie: $mov"
					mov = null
				} else if (score.mov >= 1 && score.tvs <= -1) {
					log.fine "Exclude Series: $tvs"
					tvs = null
				}
			}
		}

	}

	// CHECK CONFLICT
	if (((mov && tvs) || (!mov && !tvs))) {
		if (failOnError) {
			fail 'Media detection failed'
		} else {
			log.fine "Unable to differentiate: [$f.name] => [$tvs] VS [$mov]"
			return [:]
		}
	}

	return [tvs: tvs, mov: mov]
}

// group entries by unique tvs/mov descriptor
groups = groups.groupBy{ group, files -> group.collectEntries{ type, query -> [type, query ? query.toString().ascii().normalizePunctuation().lower() : null] } }.collectEntries{ group, maps -> [group, maps.values().flatten()] }

// log movie/series/anime detection results
groups.each{ group, files -> log.finest "Group: $group => ${files*.name}" }

// keep track of unsorted files or files that could not be processed for some reason
def unsortedFiles = []

// process each batch
groups.each{ group, files ->
	// fetch subtitles (but not for anime)
	if (group.anime == null && subtitles != null && files.findAll{ it.isVideo() }.size() > 0) {
		subtitles.each{ languageCode ->
			def subtitleFiles = getMissingSubtitles(file: files, lang: languageCode, strict: true, output: 'srt', encoding: 'UTF-8', format: 'MATCH_VIDEO_ADD_LANGUAGE_TAG') ?: []
			files += subtitleFiles
			input += subtitleFiles // make sure subtitles are added to the exclude list and other post processing operations
			temporaryFiles += subtitleFiles // if downloaded for temporarily extraced files delete later
		}
	}
	
	if (group.rio) {
		def inFiles = (input - getRenameLog().keySet())
		if (inFiles.size() > 0) {
			log.info "Processing ${inFiles.size()} rio files"
			rename(map: inFiles.collectEntries{ original ->
				def destination = getMediaInfo(original, rioFormat) as File
				return [original, destination.isAbsolute() ? destination : outputFolder.resolve(destination.path)]
			})
		}
	}	
	
	else if (group.games) {
		def inFiles = (input - getRenameLog().keySet())
		if (inFiles.size() > 0) {
			log.info "Processing ${inFiles.size()} Game files"
			rename(map: inFiles.collectEntries{ original ->
				def destination = getMediaInfo(original, gamesFormat) as File
				return [original, destination.isAbsolute() ? destination : outputFolder.resolve(destination.path)]
			})
		}
	}	
	
	else if (group.ebook) {		
		log.info "Processing ${files.size()} eBook files"
		rename(map: files.collectEntries{ original ->
			def destination = getMediaInfo(original, ebookFormat) as File
			return [original, destination.isAbsolute() ? destination : outputFolder.resolve(destination.path)]
		})
	}	

	else if (group.comics) {
		def inFiles = (input - getRenameLog().keySet())
		if (inFiles.size() > 0) {
			log.info "Processing ${inFiles.size()} Comic files"
			rename(map: inFiles.collectEntries{ original ->
				def destination = getMediaInfo(original, comicsFormat) as File
				return [original, destination.isAbsolute() ? destination : outputFolder.resolve(destination.path)]
			})
		}
	}	
	

	// EPISODE MODE
	else if ((group.tvs || group.anime) && !group.mov) {
		// choose series / anime
		def dest = group.tvs ? rename(file: files, format: seriesFormat, db: 'TheTVDB') : rename(file: files, format: animeFormat, db: 'AniDB')

		if (dest != null) {
			if (artwork) {
				dest.mapByFolder().each{ dir, fs ->
					def hasSeasonFolder = any{ dir =~ /Specials|Season.\d+/ || dir.parentFile.structurePathTail.listPath().size() > 0 }{ false }	// MAY NOT WORK FOR CERTAIN FORMATS

					fs.findResults{ it.metadata }.findAll{ it.seriesInfo.database == 'TheTVDB' }.collect{ [name: it.seriesName, season: it.special ? 0 : it.season, id: it.seriesInfo.id] }.unique().each{
						log.fine "Fetching series artwork for [$it.name / Season $it.season] to [$dir]"
						fetchSeriesArtworkAndNfo(hasSeasonFolder ? dir.parentFile : dir, dir, it.id, it.season, false, _args.locale)
					}
				}
			}
		} else if (failOnError) {
			fail "Failed to process group: $group"
		} else {
			unsortedFiles += files
		}
	}

	// MOVIE MODE
	else if (group.mov && !group.tvs && !group.anime) {
		def dest = rename(file: files, format: movieFormat, db: 'TheMovieDB')

		if (dest != null) {
			if (artwork) {
				dest.mapByFolder().each{ dir, fs ->
					def movieFile = fs.findAll{ it.isVideo() || it.isDisk() }.sort{ it.length() }.reverse().findResult{ it }
					if (movieFile) {
						def movieInfo = movieFile.metadata
						log.fine "Fetching movie artwork for [$movieInfo] to [$dir]"
						fetchMovieArtworkAndNfo(dir, movieInfo, movieFile, extras, false, _args.locale)
					}
				}
			}
		} else if (failOnError) {
			fail "Failed to process group: $group"
		} else {
			unsortedFiles += files
		}
	}

	// MUSIC MODE
	else if (group.music) {
		def dest = rename(file: files, format: musicFormat, db: 'ID3')

		if (dest != null) {
			// music artwork not supported
		} else if (failOnError) {
			fail "Failed to process group: $group"
		} else {
			unsortedFiles += files
		}
	}

	// UNSORTED
	else {
		unsortedFiles += files
	}
}


// ---------- POST PROCESSING ---------- //

// deal with remaining files that cannot be sorted automatically
if (unsorted) {
	if (unsortedFiles.size() > 0) {
		log.fine "Processing ${unsortedFiles.size()} unsorted files"
		rename(map: unsortedFiles.collectEntries{ original ->
			def destination = getMediaInfo(original, unsortedFormat) as File
			return [original, destination.isAbsolute() ? destination : outputFolder.resolve(destination.path)]
		})
	}
}

// run program on newly processed files
if (exec) {
	getRenameLog().collect{ from, to -> getMediaInfo(to, exec) }.unique().each{ command ->
		log.fine "Execute: $command"
		execute(command)
	}
}


// ---------- REPORTING ---------- //


if (getRenameLog().size() > 0) {
	// messages used for kodi / plex / emby pushover notifications
	def getNotificationTitle = {
		def count = getRenameLog().count{ k, v -> !v.isSubtitle() }
		return "FileBot finished processing $count files"
	}.memoize()

	def getNotificationMessage = { prefix = '• ', postfix = '\n' -> 
		return ut.title ?: (input.findAll{ !it.isSubtitle() } ?: input).collect{ relativeInputPath(it) as File }.root.nameWithoutExtension.unique().collect{ prefix + it }.join(postfix).trim()
	}.memoize()

	// make Kodi scan for new content and display notification message
	if (kodi) {
		kodi.each{ instance ->
			log.fine "Notify Kodi: $instance"
			tryLogCatch {
				showNotification(instance.host, instance.port ?: 8080, getNotificationTitle(), getNotificationMessage(), 'http://app.filebot.net/icon.png')
				scanVideoLibrary(instance.host, instance.port ?: 8080)
			}
		}
	}

	// make Plex scan for new content
	if (UsePlex) {
		log.fine "Notify Plex: $PlexHost Using Token: $PlexToken"
		tryLogCatch {
			def protocol = PlexHost ==~ /localhost|[0-9.:]+/ ? 'http' : 'https'
			def url = "$protocol://$PlexHost:$PlexPort/library/sections/all/refresh"
			if (PlexToken) {
				url += "?X-Plex-Token=$PlexToken"
			}
		log.finest "GET: $url"
		new URL(url).get()
//			refreshPlexLibrary(PlexHost, PlexPort, PlexToken)
		}
	}

	// make Emby scan for new content
	if (emby) {
		emby.each{ instance ->
			log.fine "Notify Emby: $instance"
			tryLogCatch {
				refreshEmbyLibrary(instance.host, 8096, instance.token)
			}
		}
	}

	// mark episodes as 'acquired'
	if (myepisodes) {
		log.fine 'Update MyEpisodes'
		tryLogCatch {
			executeScript('update-mes', [login:myepisodes.join(':'), addshows:true], getRenameLog().values())
		}
	}

	if (pushover) {
		log.fine 'Sending Pushover notification'
		tryLogCatch {
			Pushover(pushover[0], pushover[1] ?: 'wcckDz3oygHSU2SdIptvnHxJ92SQKK').send(getNotificationTitle(), getNotificationMessage())
		}
	}

	// messages used for email / pushbullet reports
	def getReportSubject = { getNotificationMessage('', ' | ') }
	def getReportTitle = { '[FileBot] ' + getReportSubject() }
	def getReportMessage = { 
		def renameLog = getRenameLog()
		'''<!DOCTYPE html>\n''' + XML {
			html {
				head {
					meta(charset:'UTF-8')
					style('''
						p{font-family:Arial,Helvetica,sans-serif}
						p b{color:#07a}
						hr{border-style:dashed;border-width:1px 0 0 0;border-color:lightgray}
						small{color:#d3d3d3;font-size:xx-small;font-weight:normal;font-family:Arial,Helvetica,sans-serif}
						table a:link{color:#666;font-weight:bold;text-decoration:none}
						table a:visited{color:#999;font-weight:bold;text-decoration:none}
						table a:active,table a:hover{color:#bd5a35;text-decoration:underline}
						table{font-family:Arial,Helvetica,sans-serif;color:#666;background:#eaebec;margin:15px;border:#ccc 1px solid;border-radius:3px;box-shadow:0 1px 2px #d1d1d1}
						table th{padding:15px;border-top:1px solid #fafafa;border-bottom:1px solid #e0e0e0;background:#ededed}
						table th{text-align:center;padding-left:20px}
						table tr:first-child th:first-child{border-top-left-radius:3px}
						table tr:first-child th:last-child{border-top-right-radius:3px}
						table tr{text-align:left;padding-left:20px}
						table td:first-child{text-align:left;padding-left:20px;border-left:0}
						table td{padding:15px;border-top:1px solid #fff;border-bottom:1px solid #e0e0e0;border-left:1px solid #e0e0e0;background:#fafafa;white-space:nowrap}
						table tr.even td{background:#f6f6f6}
						table tr:last-child td{border-bottom:0}
						table tr:last-child td:first-child{border-bottom-left-radius:3px}
						table tr:last-child td:last-child{border-bottom-right-radius:3px}
						table tr:hover td{background:#f2f2f2}
					''')
					title(getReportTitle())
				}
				body {
					p {
						mkp.yield("FileBot finished processing ")
						b(getReportSubject())
						mkp.yield(" (${renameLog.size()} files).")
					}
					hr(); table {
						tr { th('Original Name'); th('New Name'); th('New Location') }
						renameLog.each{ from, to ->
							tr { [from.name, to.name, to.parent].each{ cell -> td(cell) } }
						}
					}
					hr(); small("// Generated by ${Settings.applicationIdentifier} on ${InetAddress.localHost.hostName} at ${now}")
				}
			}
		}
	}

	// store processing report
	if (storeReport) {
		def reportFolder = ApplicationFolder.AppData.resolve('reports')
		def reportName = [now.format(/[yyyy-MM-dd HH mm]/), getReportSubject().take(50)].join(' ').validateFileName().space('_')
		def reportFile = getReportMessage().saveAs(reportFolder.resolve(reportName + '.html'))
		log.finest "Saving report as ${reportFile}"
	}

	// send pushbullet report
	if (pushbullet) {
		log.fine 'Sending PushBullet report'
		tryLogCatch {
			PushBullet(pushbullet).sendFile(getNotificationTitle(), getReportMessage(), 'text/html', getNotificationMessage(), any{ mailto }{ null })
		}
	}

	// send email report
	if (gmail || mail) {
		tryLogCatch {
			sendEmailReport(getReportTitle(), getReportMessage(), 'text/html')
		}
	}
}


// ---------- CLEAN UP ---------- //


// clean up temporary files that may be left behind after extraction
if (deleteAfterExtract) {
	extractedArchives.each{ a ->
		log.finest "Delete archive $a"
		a.delete()
		a.dir.listFiles().toList().findAll{ v -> v.name.startsWith(a.nameWithoutExtension) && v.extension ==~ /r\d+/ }.each{ v ->
			log.finest "Delete archive volume $v"
			v.delete()
		}
	}
}

// clean empty folders, clutter files, etc after move
if (clean) {
	if (['DUPLICATE', 'COPY', 'HARDLINK'].any{ it.equalsIgnoreCase(_args.action) } && temporaryFiles.size() > 0) {
		log.fine 'Clean temporary extracted files'
		// delete extracted files
		temporaryFiles.findAll{ it.isFile() }.sort().each{
			log.finest "Delete $it"
			it.delete()
		}
		// delete remaining empty folders
		temporaryFiles.findAll{ it.isDirectory() }.sort().reverse().each{
			log.finest "Delete $it"
			if (it.getFiles().size() == 0) {
				it.deleteDir()
			}
		}
	}

	// deleting remaining files only makes sense after moving files
	if ('MOVE'.equalsIgnoreCase(_args.action)) {
		def cleanerInput = args.size() > 0 ? args : ut.kind == 'multi' && ut.dir ? [ut.dir as File] : []
		cleanerInput = cleanerInput.findAll{ f -> f.exists() }
		if (cleanerInput.size() > 0) {
			log.fine 'Clean clutter files and empty folders'
			executeScript('cleaner', args.size() == 0 ? [root:true, ignore: ignore] : [root:false, ignore: ignore], cleanerInput)
		}
	}
}



if (getRenameLog().size() == 0) {
	fail "Finished without processing any files"
}